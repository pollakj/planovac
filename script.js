


// Script pro funkčnost stránky s úkoly + splněnými úkoly
// Zajišťuje funkčnost tlačítek a ukládání úkolů do localstorage


let todo = JSON.parse(localStorage.getItem("todo")) || [];
let completedTasks = JSON.parse(localStorage.getItem("completedTasks")) || [];

// Funkce pro uložení dat do localStorage
function ulozitData() {
    localStorage.setItem("todo", JSON.stringify(todo));
    localStorage.setItem("completedTasks", JSON.stringify(completedTasks));
}

// Funkce pro stránku s úkoly
function initTodoPage() {
    const todoInput = document.getElementById("todoInput");
    const todoDate = document.getElementById("todoDate");
    const todoList = document.getElementById("todoList");
    const addButton = document.getElementById("addButton");
    const deleteButton = document.getElementById("deleteButton");

    addButton.addEventListener("click", pridejUkol);
    todoInput.addEventListener("keydown", function (event) {
        if (event.key === "Enter") {
            event.preventDefault();
            pridejUkol();
        }
    });
    deleteButton.addEventListener("click", smazatVse);
    zobrazUkol();

    //@par pridejUkol: ořízne přebytečné mezery a pokud pole pro vložení úkolu není prázdné, přidá nový úkol + následně smaže jak pole pro vložení úkolu, tak i datum

    function pridejUkol() {
        const newTask = todoInput.value.trim();  // oříznutí přebytečných mezer
        const dueDate = todoDate.value.trim();
        if (newTask !== "") { //pokud není pole prázdný, přidá se
            todo.push({ text: newTask, date: dueDate, disabled: false });
            ulozitData();
            todoInput.value = ""; //vyprázdění pole + datumu
            todoDate.value = "";
            zobrazUkol();
        }
    }

    //@par zobrazUkol: Vymaže všechny úkoly a následně je znovu načte, aby se neduplikovali úkoly a upravuje vhled listu úkolů pomocí HTML a script tam kontroluje zda je úkol splněný nebo ne a podle toho přidáva vzhled úkolu

    function zobrazUkol() {
        todoList.innerHTML = "";  // vymazání aktualního obsahu
        todo.forEach((item, index) => {
            const p = document.createElement("p");
            p.innerHTML = //vytvoření HTML pro jednotlivé úkoly
            `<div class="todo-container">  
                    <input type="checkbox" class="todo-checkbox" id="input-${index}" ${item.disabled ? "checked" : ""}>
                    <span id="todo-${index}" class="${item.disabled ? "disabled" : ""}" onclick="upravUkol(${index})">${item.text}</span>
                    <span class="todo-date">${item.date}</span>
                    <button class="delete-btn" data-index="${index}">&times;</button>
              </div>`;
            p.querySelector(".todo-checkbox").addEventListener("change", () => oznacUkol(index));  //aby fungovali funkce "splněný úkol/smazat úkol"
            p.querySelector(".delete-btn").addEventListener("click", () => smazatUkol(index));
            todoList.appendChild(p);
        });
    }

    //@par oznacUkol: Kontroluje zda je úkol spněný nebo ne a podle toho úkol přesune na stránku se splněnými úkoly

    function oznacUkol(index) {     //kontrola, jestli je ukol splněn
        todo[index].disabled = !todo[index].disabled;  
        if (todo[index].disabled) {
            completedTasks.push(todo[index]);
            todo.splice(index, 1);
        }
        ulozitData();
        zobrazUkol();
    }

    //@par smazatUkol: Smaže úkol

    function smazatUkol(index) {
        todo.splice(index, 1);
        ulozitData();
        zobrazUkol();
    }

    //@par smazatVse: Smaže všechny úkoly

    function smazatVse() {
        todo = [];
        ulozitData();
        zobrazUkol();
    }
}

// Funkce pro stránku se splněnými úkoly
function initCompletedPage() {
    const completedTasksList = document.getElementById("completedTasksList");
    const backButton = document.getElementById("backButton");
    const deleteAllButton = document.getElementById("deleteAllButton");

    zobrazSplneneUkoly();

    backButton.addEventListener("click", function() {
        window.location.href = 'index.html';
    });

    deleteAllButton.addEventListener("click", smazatVsechnySplneneUkoly);

    //@par zobrazSplneneUkoly: Vyprázdní list se splnenými úkoly a znovu je načte, aby nedocházelo k duplikaci úkolů a pomocí vnitřního HTML přidáva vzhled položkám v seznamu 

    function zobrazSplneneUkoly() {
        completedTasksList.innerHTML = "";
        completedTasks.forEach((item, index) => {
            const row = document.createElement("tr");
            row.innerHTML = `
                <td>${item.text}</td>
                <td><button class="delete-btn" data-index="${index}">&times;</button></td>
            `;
            row.querySelector(".delete-btn").addEventListener("click", () => smazatSplneneUkol(index));
            completedTasksList.appendChild(row);
        });
    }

    //@par smazatSplneneUkol: Vymaže vybraný úkol pomocí tlačítka

    function smazatSplneneUkol(index) {
        completedTasks.splice(index, 1);
        ulozitData();
        zobrazSplneneUkoly();
    }

    //@par smazatVsechnySplneneUkoly: Vyprázdní pole se splněnými úkoly

    function smazatVsechnySplneneUkoly() {
        completedTasks = [];
        ulozitData();
        zobrazSplneneUkoly();
    }
}

// Inicializace podle aktuální stránky
document.addEventListener("DOMContentLoaded", function () {
    if (document.getElementById("todoList")) {
        initTodoPage();
    } else if (document.getElementById("completedTasksList")) {
        initCompletedPage();
    }
});
